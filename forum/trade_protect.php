<?php

/**
 * @ignore
 */
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);


$user->session_begin();
$auth->acl($user->data);
$user->setup();

$mode = request_var('mode', '');
$topic_id = request_var('t', 0);


if ($user->data['user_id'] == ANONYMOUS) {
    login_box('', $user->lang['LOGIN_TRADE']);
}


if ($mode == 'post') {
    $username = strtolower(request_var('username', ''));
    $message = request_var('message', '');

    $sql = "SELECT * FROM " . USERS_TABLE . " WHERE `username_clean`='{$username}'";
    $result = $db->sql_query($sql);

    if ($row = $db->sql_fetchrow($result)) {
        if($row['user_id']!=$user->data['user_id']) {
            $sql = "INSERT INTO " . TRADE_PROTECT_MESSAGES . " ( `buyer_id`, `seller_id`,`message`) 
        VALUES ({$user->data['user_id']},{$row['user_id']},'{$message}')";
            $result = $db->sql_query($sql);
            $template->assign_vars(array(
                "L_POSTED" => true,
                "U_POSTED" => "Ваш запрос успешно отправлен!"
            ));
        }
        else{
            $template->assign_vars(array(
                "L_INVALID_SELLER"  => true,
                "U_INVALID_SELLER"  => "Нельзя содать запрос на покупку у себя же"
            ));
        }

    } else {
        $template->assign_vars(array(
            "L_USER_NOT_FOUND"  => true,
            "U_USER_NOT_FOUND"  => $user->lang['USER_NOT_FOUND_OR_INACTIVE']
        ));
    }
}
if ($topic_id != 0) {
    $sql = "SELECT * FROM " . TOPICS_TABLE . " WHERE topic_id = {$topic_id}";
    $result = $db->sql_query($sql);
    $topic = $db->sql_fetchrow($result);
    $db->sql_freeresult($result);
    if ($topic['trade_type'] == 0) {
        trigger_error('TOPIC_UNTRADE');

    }

    $sql = "SELECT * FROM " . USERS_TABLE . " WHERE user_id = {$topic['topic_poster']}";
    $result = $db->sql_query($sql);
    $topic_poster = $db->sql_fetchrow($result);
    $db->sql_freeresult($result);

    $template->assign_vars(array(
        "L_USER_SALER" => true,
        "USER_SALER" => get_username_string('username', $topic_poster['user_id'], $topic_poster['username'], $topic_poster['user_colour'], ""),
        "U_USER_SALER" => get_username_string('profile', $topic_poster['user_id'], $topic_poster['username'], $topic_poster['user_colour'], "")
    ));
} else {
    $template->assign_vars(array(
            "L_USER_SALER" => false
        )
    );
}

$template->assign_var("L_TITLE", "Trade Protect");
$vars = array('page_title');
extract($phpbb_dispatcher->trigger_event('core.index_modify_page_title', compact($vars)));
page_header("Trade Protect");


$template->set_filenames(array(
        'body' => 'trade_protect_body.html')
);
make_jumpbox(append_sid("{$phpbb_root_path}viewforum.$phpEx"));

page_footer();