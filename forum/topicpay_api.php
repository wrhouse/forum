<?php
/**
 * @ignore
 */
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);


$user->session_begin();
$auth->acl($user->data);
$user->setup();

$user_id = $user->data['user_id'];
if ($user_id == ANONYMOUS)
    exit();
$type = request_var('type', '');

if ($type != '') {
    $topic_id = request_var('topic_id', 0);
    switch ($type) {
        case 'getTypes':
            $res = '<select style="
    border-radius: 100px;
    padding: 5px 20px;
 
" id="typeList" onchange="getTypeProperty(' . $topic_id . ');"> <option value="0">Выбрать...</option>';
            $sql = "SELECT * FROM " . TOPICS_COLOR . " WHERE topic_id=" . $topic_id . " AND buy_time+duration>CURRENT_TIMESTAMP() ";
            $result = $db->sql_query($sql);
            if ($row = $db->sql_fetchrow($result)) {
                $res .= '<option value="11">Изменить цвет топика</option>';
            } else {
                $res .= '<option value="1">Выбрать цвет топика</option>';

            }
            $db->sql_freeresult($result);
            $sql = "SELECT * FROM " . TOPICS_FIXED . " WHERE topic_id=" . $topic_id . " AND estimated_time>CURRENT_TIMESTAMP()";
            $result = $db->sql_query($sql);
            if ($row = $db->sql_fetchrow($result)) {
                $res .= '<option value="21">Продлить закреп</option>';
                $sql2 = "SELECT * FROM " . TOPICS_FIXED . " WHERE forum_id=" . $row['forum_id'] . " AND estimated_time>CURRENT_TIMESTAMP() ORDER BY buy_time DESC";
                $result2 = $db->sql_query($sql);
                $row2 = $db->sql_fetchrow($result2);
                if ($row2['id'] != $row['id']) {
                    $res .= '<option value="22">Поднять среди закрепов</option>';
                }
                $db->sql_freeresult($result2);
            } else {
                $res .= '<option value="2">Закрепить топик</option>';

            }
            $db->sql_freeresult($result);
            $res .= '<option value="3">Поднять топик в выдаче</option>';
            echo $res;
            break;
        case 'getTypeProperties':
            $property = request_var('typeId', 0);
            $result_arr = array();
            $res = '<form id="property" onsubmit="setProperty(' . $topic_id . '); return false;">';
            switch ($property) {
                case 1:
                    $res .= '<input type="hidden" value="1" name="propertyId">';
                    $res .= '<input type="radio" name="color" value="1">#5fd29e';
                    $res .= '<input type="radio" name="color" value="2">#f99cb4 <br>';
                    $res .= '<input type="radio" name="duration" value="1">3 Дня';
                    $res .= '<input type="radio" name="duration" value="2">7 Дней <br>';
                    $res .= '<input style="
    border-radius: 100px;
    padding: 5px 20px;
" type="submit" value="Применить">';

                    break;
                case 11:
                    $res .= '<input type="hidden" value="11" name="propertyId">';
                    $res .= '<input type="radio" name="color" value="1">#5fd29e';
                    $res .= '<input type="radio" name="color" value="2">#f99cb4 <br>';
                    $res .= '<input style="
    border-radius: 100px;
    padding: 5px 20px;
" type="submit" value="Изменить">';

                    break;
                case 2:
                    $res .= '<input type="hidden" value="2" name="propertyId">';
                    $res .= '<input type="radio" name="duration" value="1">3 Дня';
                    $res .= '<input type="radio" name="duration" value="2">7 Дней<br>';
                    $res .= '<input style="
    border-radius: 100px;
    padding: 5px 20px;
" type="submit" value="Закрепить">';

                    break;
                case 21:
                    $res .= '<input type="hidden" value="21" name="propertyId">';
                    $res .= '<input type="radio" name="duration" value="1">1 День';
                    $res .= '<input type="radio" name="duration" value="2">3 Дня';
                    $res .= '<input type="radio" name="duration" value="3">7 Дней<br>';
                    $res .= '<input style="
    border-radius: 100px;
    padding: 5px 20px;
 
" type="submit" value="Продлить">';

                    break;
                case 22:
                    $res .= '<input type="hidden" value="22" name="propertyId">';
                    $res .= '<input style="
    border-radius: 100px;
    padding: 5px 20px;
 
" type="submit" value="Поднять">';

                    break;
                case 3:
                    $res .= '<input type="hidden" value="22" name="propertyId">';
                    $res .= '<input style="
    border-radius: 100px;
    padding: 5px 20px;
 
" type="submit" value="Поднять">';
                    break;
            }
            $res .= '</form>';
            echo $res;
            break;
        case 'setProperty':
            $property = request_var('propertyId', 0);
            switch ($property) {
                case 1:
                    $color = request_var('color', '');
                    $duration = request_var('duration', 0);
                    $sql_dur = '';
                    if ($duration == 1) {
                        $sql_dur = "DATE_ADD(NOW(), INTERVAL 3 DAY)";
                    } else if ($duration == 2) {
                        $sql_dur = "DATE_ADD(NOW(), INTERVAL 7 DAY)";
                    }

                    $sql = "INSERT INTO " . TOPICS_COLOR . " (`topic_id`,`color`,`duration`) 
                    VALUES (" . $topic_id . ",'" . $color . "', " . $sql_dur . ")";
                    $db->sql_query($sql);
                    break;
                case 11:
                    $color = request_var('color', '');
                    $sql = "UPDATE " . TOPICS_COLOR . " SET `color`='" . $color . "' WHERE `topic_id`=" . $topic_id;
                    $db->sql_query($sql);
                    break;
                case 2:
                    $duration = request_var('duration', 0);
                    $sql_dur = '';
                    if ($duration == 1) {
                        $sql_dur = "DATE_ADD(NOW(), INTERVAL 3 DAY)";
                    } else if ($duration == 2) {
                        $sql_dur = "DATE_ADD(NOW(), INTERVAL 7 DAY)";
                    }
                    $sql = "SELECT * FROM " . TOPICS_TABLE . " WHERE topic_id=" . $topic_id;
                    $result = $db->sql_query($sql);
                    $row = $db->sql_fetchrow($result);
                    $forum_id = $row['forum_id'];
                    $db->freeresult($result);

                    $sql = "INSERT INTO " . TOPICS_FIXED . " (`topic_id`,`forum_id`,`user_id`,`estimated_time`)
                     VALUES (" . $topic_id . "," . $forum_id . "," . $user_id . ", " . $sql_dur . ")";
                    $db->sql_query($sql);

                    break;
                case 21:

                    $duration = request_var('duration', 0);
                    $sql_dur = '';
                    if ($duration == 1) {
                        $sql_dur = "DATE_ADD(estimated_time, INTERVAL 1 DAY)";
                    } else if ($duration == 2) {
                        $sql_dur = "DATE_ADD(estimated_time, INTERVAL 3 DAY)";
                    } else if ($duration == 3) {
                        $sql_dur = "DATE_ADD(estimated_time, INTERVAL 7 DAY)";
                    }

                    $sql = "UPDATE " . TOPICS_FIXED . " SET estimated_time=".$sql_dur." WHERE `topic_id`=".$topic_id;
                    $db->sql_query($sql);
                    break;
                case 22:
                    $sql = "UPDATE " . TOPICS_FIXED . " SET `buy_time`=NOW() WHERE `topic_id`=".$topic_id;
                    $db->sql_query($sql);
                    break;
                case 3:
                    //TODO UP TOPIC
                    $sql = "SELECT * FROM " . TOPICS_TABLE . " WHERE topic_id=" . $topic_id;
                    $result = $db->sql_query($sql);
                    $row = $db->sql_fetchrow($result);
                    $forum_id = $row['forum_id'];
                    $db->freeresult($result);
                    $sql = 'SELECT f.*, t.* FROM '.TOPICS_TABLE.' t, '.FORUMS_TABLE ." f WHERE t.topic_id = $topic_id 
                    AND f.forum_id = t.forum_id";
                    $result = $db->sql_query($sql);
                    $post_data = $db->sql_fetchrow($result);
                    $db->sql_freeresult($result);
                    $current_time = time();
                    phpbb_bump_topic($forum_id, $topic_id, $post_data, $current_time);
                    break;
            }
            break;

    }
}


