<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 19.06.2019
 * Time: 17:17
 */


class ucp_pay
{

    var $u_action;

    function main($id, $mode)
    {
        global $cache, $config, $db, $user, $auth, $template, $phpbb_root_path, $phpEx;
        global $request, $phpbb_container, $phpbb_dispatcher;
        $submit = $request->variable('submit', false, false, \phpbb\request\request_interface::POST);

        switch ($mode) {
            case 'balance':
                $type = request_var('type','');
                $need = request_var('need',0);
                $template->assign_var('L_TITLE', 'Баланс');
                if ($submit) {
                    $coins = $request->variable('coins', '', true);
                    $sql = 'UPDATE ' . USERS_TABLE . '
								SET coins=coins + ' . $coins . '
								WHERE user_id = ' . $user->data['user_id'];
                    $db->sql_query($sql);
                }
                if($type=='not_enough'){
                    $template->assign_vars(array(
                        "ERROR" => true,
                        "MESSAGE"   => 'Недостаточно средств, пожалуйста пополните на сумму не менее '.$need,
                        "NEED"      => $need
                    ));
                }
                $sql = 'SELECT  * 
						FROM ' . USERS_TABLE . '
						WHERE  ' . $db->sql_in_set('user_id', $user->data['user_id']);
                $result = $db->sql_query($sql);
                $row = $db->sql_fetchrow($result);
                $template->assign_var('L_COINS',$row['coins'] .' Коинов');

                $template->assign_var('L_COINS_2',$row['coins'] .' Коинов');

                break;
            case 'bills':
                $template->assign_var('L_TITLE', 'Счета на оплату');
                $type = request_var('type','');
                $garant_id= request_var('g',0);

                    if($type!='' && $garant_id!=0){
                        $sql_user = "SELECT * FROM ". USERS_TABLE . " WHERE user_id={$user->data['user_id']}";
                        $result_user = $db->sql_query($sql_user);
                        $row = $db->sql_fetchrow($result_user);
                        $sql_garant = "SELECT * FROM ". TRADE_PROTECT_MESSAGES . " WHERE id={$garant_id}";
                        $result_garant = $db->sql_query($sql_garant);
                        $garant = $db->sql_fetchrow($result_garant);
                        //print_r($garant_id);
                        if($row['coins']>=$garant['good_price']+$garant['garant_price']) {
                            $sql = "UPDATE " . TRADE_PROTECT_MESSAGES . " SET status=3 WHERE id={$garant_id}";
                            $db->sql_query($sql);
                            $sql = 'UPDATE ' . USERS_TABLE . '
								SET coins=coins - ' . ($garant['good_price']+$garant['garant_price']) . '
								WHERE user_id = ' . $user->data['user_id'];
                            $db->sql_query($sql);
                            $sql = 'UPDATE ' . USERS_TABLE . '
								SET coins=coins + ' . ($garant['good_price']+$garant['garant_price']) . '
								WHERE user_id = ' . $garant['garant_id'];
                            $db->sql_query($sql);
                        }else{
                            $need = $garant['good_price']+$garant['garant_price'] - $row['coins'];
                            redirect(append_sid("{$phpbb_root_path}ucp.$phpEx","i=ucp_pay&mode=balance&type=not_enough&need={$need}"));
                        }
                    }


                $sql = 'SELECT * FROM ' . TRADE_PROTECT_MESSAGES . " WHERE buyer_id={$user->data['user_id']} AND status=2";
                $result = $db->sql_query($sql);
                while($row = $db->sql_fetchrow($result)){
                    $template->assign_block_vars("bills",array(
                        "GOOD"      => $row['good'] . " + оплата гаранту" ,
                        "PRICE"     => $row['good_price']. " + ".$row['garant_price'],
                        "L_PAY_BUTTON"  => append_sid("{$phpbb_root_path}ucp.$phpEx","i=ucp_pay&mode=bills&g={$row['id']}&type=pay")
                    ));
                }

                break;
        }


        $template->assign_var('S_UCP_ACTION', $this->u_action);
        $this->tpl_name = 'ucp_pay_' . $mode;
        $this->page_title = 'UCP_PAY_' . strtoupper($mode);

    }


}