<?php
// phpBB 3.1.x auto-generated configuration file
// Do not change anything in this file!
$dbms = 'phpbb\\db\\driver\\mysqli';
$dbhost = $_ENV['DATABASE_HOST'];
$dbport = $_ENV['DATABASE_PORT'];
$dbname = $_ENV['DATABASE_NAME'];
$dbuser = $_ENV['DATABASE_USER'];
$dbpasswd = $_ENV['DATABASE_PASSWORD'];
$table_prefix = 'phpbb_';
$phpbb_adm_relative_path = 'adm/';
$acm_type = 'phpbb\\cache\\driver\\file';

@define('PHPBB_INSTALLED', true);
// @define('PHPBB_DISPLAY_LOAD_TIME', true);
// @define('DEBUG', false);
// @define('DEBUG_CONTAINER', true);

