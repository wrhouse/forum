<?php

/**
 * @ignore
 */
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

$topic_id = request_var('t', 0);

$user->session_begin();
$auth->acl($user->data);
$user->setup();
if($user->data['user_id'] == ANONYMOUS){
    login_box('', $user->lang['LOGIN_TRADE']);
}

$flag = false;
if ($topic_id != 0) {
    $sql = "SELECT * FROM " . TOPICS_TABLE . " WHERE topic_id=" . $topic_id;
    $result = $db->sql_query($sql);
    $row = $db->sql_fetchrow($result);
    $db->sql_freeresult($result);
    if ($user->data['user_id'] != $row['topic_poster'] || $row['trade_type'] == 0)
        $flag = true;
}
if ($topic_id == 0 || $flag) {
    $sql = "SELECT * FROM " . TOPICS_TABLE . " WHERE topic_poster=" . $user->data['user_id'] . " AND trade_type!=0";
    $result = $db->sql_query($sql);
    while ($row = $db->sql_fetchrow($result)) {
        $template->assign_block_vars('topics', $row);
    }
    $db->sql_freeresult($result);
    $template->assign_var('L_TOPIC_BLOCKED', false);

} else {
    $sql = "SELECT * FROM " . TOPICS_TABLE . " WHERE topic_poster=" . $user->data['user_id'] . " AND trade_type!=0 and topic_id=" . $topic_id;
    $result = $db->sql_query($sql);
    $row = $db->sql_fetchrow($result);
    $db->sql_freeresult($result);
    $row['SELECTED'] = true;
    $template->assign_block_vars('topics', $row);
    $template->assign_var('L_TOPIC_BLOCKED', true);
}


//TODO вывод всех типов платного фукнционала

$template->assign_block_vars('paytype', array(
    'id' => 1,
    'name' => 'Закрепить топик'
));
$template->assign_block_vars('paytype', array(
    'id' => 2,
    'name' => 'Поднять топик в выдаче'
));

$template->assign_block_vars('paytype', array(
    'id' => 4,
    'name' => 'Выбрать цвет для топика'
));
//TODO принятие данных с формы и их обработка


$template->assign_var("L_TITLE", "Платный функционал");
$vars = array('page_title');
extract($phpbb_dispatcher->trigger_event('core.index_modify_page_title', compact($vars)));
page_header("Пдатный функционал");


$template->set_filenames(array(
        'body' => 'topicpay_body.html')
);
make_jumpbox(append_sid("{$phpbb_root_path}viewforum.$phpEx"));

page_footer();

?>
