$(document).ready(function() {
    let $elem = $('.forum_symbol');
    let fwds = new Array($elem.length);
    let counters = new Array($elem.length);

    for(let it = 0;it<fwds.length;it++){
        fwds[it] = true;
        counters[it] = 0;
    }
    let timer = null;
    let speed = 4;
    $elem.on('click',function () {
        let elem = $(this)
        let index = $elem.index($(this));
        let forum_list = $('ul.topiclist.forums').eq(index);
        if (fwds[index]) {
            forum_list.animate({height: 'hide'}, 500);
        } else {
            forum_list.animate({height: 'show'}, 500);
        }

        clearInterval(timer);
        timer = setInterval(function () {
            if (!fwds[index]) {
                counters[index] += speed;
            } else {
                counters[index] -= speed;
            }

            if (counters[index] < 0) {
                counters[index] = 0;
                clearInterval(timer);
            }

            if (counters[index] > 90) {
                counters[index] = 90;
                clearInterval(timer);
            }

            elem.css("transform", "rotate(" + counters[index] + "deg)");
        }, 20);
        fwds[index] = !fwds[index];
    });

});
