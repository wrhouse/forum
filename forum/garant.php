<?php
/**
 * @ignore
 */
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);


$user->session_begin();
$auth->acl($user->data);
$user->setup();

$mode = request_var('mode', 'list');
$type = request_var('type', '');


$sql = "SELECT * FROM " . USER_GROUP_TABLE . " WHERE user_id={$user->data['user_id']} AND group_id=8";
$result = $db->sql_query($sql);

if ($user->data['user_id'] == ANONYMOUS || !$row = $db->sql_fetchrow($result)) {
    login_box('', $user->lang['LOGIN_GARANT']);
}
$db->sql_freeresult($result);
$garant_id = request_var('i', 0);


switch ($type) {
    case 'take':
        if ($garant_id != 0) {
            $sql = "UPDATE " . TRADE_PROTECT_MESSAGES . " SET status=1, garant_id={$user->data['user_id']} WHERE id={$garant_id}";
            $db->sql_query($sql);
            redirect(append_sid("{$phpbb_root_path}garant.$phpEx","type=create_invoice&i={$garant_id}"));
        }
        break;
    case 'create_invoice':
        if ($garant_id != 0) {
            $good = request_var('good', '');
            $good_price = request_var('good_price', 0);
            $garant_price = request_var('garant_price', 0);
            if ($good == '' || $good_price == 0 || $garant_price == 0) {
                $template->assign_vars(array(
                    "L_INVOICE" =>append_sid("{$phpbb_root_path}garant.$phpEx","type=create_invoice&i={$garant_id}")
                ));
            } else {
                $sql = "UPDATE " . TRADE_PROTECT_MESSAGES . " SET status=2,good='{$good}',good_price ='{$good_price}',garant_price={$garant_price} WHERE id={$garant_id}";
                $db->sql_query($sql);
                redirect(append_sid("{$phpbb_root_path}garant.$phpEx","mode=mine"));
            }
        }
        break;
    case 'exit_invoice':
        if($garant_id!=0){
            $sql = "UPDATE ". TRADE_PROTECT_MESSAGES . " SET status=4 WHERE id={$garant_id}";
            $db->sql_query($sql);
            redirect(append_sid("{$phpbb_root_path}garant.$phpEx","mode=mine"));
        }
        break;
}


switch ($mode) {
    case 'list':
        $sql = "SELECT * FROM " . TRADE_PROTECT_MESSAGES . " WHERE status=0";
        $result = $db->sql_query($sql);
        while ($row = $db->sql_fetchrow($result)) {
            $sql_buyer = "SELECT * FROM " . USERS_TABLE . " WHERE user_id={$row['buyer_id']}";
            $result_buyer = $db->sql_query($sql_buyer);
            $buyer = $db->sql_fetchrow($result_buyer);
            $sql_seller = "SELECT * FROM " . USERS_TABLE . " WHERE user_id={$row['seller_id']}";
            $result_seller = $db->sql_query($sql_seller);
            $seller = $db->sql_fetchrow($result_seller);
            $template->assign_block_vars("messages",
                array(
                    "U_BUYER" => get_username_string('username', $buyer['user_id'], $buyer['username'], $buyer['user_colour'], ""),
                    "L_BUYER" => get_username_string('profile', $buyer['user_id'], $buyer['username'], $buyer['user_colour'], ""),
                    "U_SELLER" => get_username_string('username', $seller['user_id'], $seller['username'], $seller['user_colour'], ""),
                    "L_SELLER" => get_username_string('profile', $seller['user_id'], $seller['username'], $seller['user_colour'], ""),
                    "MESSAGE" => $row['message'],
                    "L_TAKE" => append_sid("{$phpbb_root_path}garant.$phpEx", "type=take&mode=mine&i={$row['id']}"),
                    "U_TAKE" => "Обработать запрос"
                ));
        }
        $db->sql_freeresult($result);
        break;
    case 'mine':
        if ($garant_id != 0) {
            $sql = "SELECT * FROM " . TRADE_PROTECT_MESSAGES . " WHERE id={$garant_id}";
            $result = $db->sql_query($sql);
            $row = $db->sql_fetchrow($result);
            $sql_buyer = "SELECT * FROM " . USERS_TABLE . " WHERE user_id={$row['buyer_id']}";
            $result_buyer = $db->sql_query($sql_buyer);
            $buyer = $db->sql_fetchrow($result_buyer);
            $sql_seller = "SELECT * FROM " . USERS_TABLE . " WHERE user_id={$row['seller_id']}";
            $result_seller = $db->sql_query($sql_seller);
            $seller = $db->sql_fetchrow($result_seller);
            $template->assign_vars(array(
                "U_BUYER" => get_username_string('username', $buyer['user_id'], $buyer['username'], $buyer['user_colour'], ""),
                "L_BUYER" => get_username_string('profile', $buyer['user_id'], $buyer['username'], $buyer['user_colour'], ""),
                "U_SELLER" => get_username_string('username', $seller['user_id'], $seller['username'], $seller['user_colour'], ""),
                "L_SELLER" => get_username_string('profile', $seller['user_id'], $seller['username'], $seller['user_colour'], ""),
                "MESSAGE" => $row['message'],
            ));
        } else {
            $sql = "SELECT * FROM " . TRADE_PROTECT_MESSAGES . " WHERE garant_id={$user->data['user_id']}";
            $result = $db->sql_query($sql);
            while ($row = $db->sql_fetchrow($result)) {
                $sql_buyer = "SELECT * FROM " . USERS_TABLE . " WHERE user_id={$row['buyer_id']}";
                $result_buyer = $db->sql_query($sql_buyer);
                $buyer = $db->sql_fetchrow($result_buyer);
                $sql_seller = "SELECT * FROM " . USERS_TABLE . " WHERE user_id={$row['seller_id']}";
                $result_seller = $db->sql_query($sql_seller);
                $seller = $db->sql_fetchrow($result_seller);
                $message = array(
                    "U_BUYER" => get_username_string('username', $buyer['user_id'], $buyer['username'], $buyer['user_colour'], ""),
                    "L_BUYER" => get_username_string('profile', $buyer['user_id'], $buyer['username'], $buyer['user_colour'], ""),
                    "U_SELLER" => get_username_string('username', $seller['user_id'], $seller['username'], $seller['user_colour'], ""),
                    "L_SELLER" => get_username_string('profile', $seller['user_id'], $seller['username'], $seller['user_colour'], ""),
                    "MESSAGE" => $row['message']
                );

                switch ($row['status']) {
                    case '1':
                        $message["U_CREATE_BILL"] = "Выдать счет";
                        $message["L_CREATE_BILL"]= append_sid("{$phpbb_root_path}garant.$phpEx","type=create_invoice&i={$row['id']}");
                        $template->assign_block_vars("messages_taken", $message);
                        break;
                    case 2:
                        $template->assign_block_vars("messages_sent_bills", $message);
                        break;
                    case 3:
                        $message['U_EXIT_INVOICE'] = "Закрыть трейд";
                        $message['L_EXIT_INVOICE'] = append_sid("{$phpbb_root_path}garant.$phpEx","type=exit_invoice&i={$row['id']}");;
                        $template->assign_block_vars("messages_payed_bills", $message);
                        break;
                    case 4:
                        $template->assign_block_vars("messages_exit", $message);
                        break;
                }
            }
        }
        break;
}

$vars = array('page_title');
extract($phpbb_dispatcher->trigger_event('core.index_modify_page_title', compact($vars)));
page_header("Trade Protect");

switch ($mode) {
    case 'list':
        $template->assign_var("L_TITLE", "Список запросов");

        $template->set_filenames(array(
                'body' => 'viewgarant_list.html')
        );
        break;
    case 'mine':
        $template->assign_var("L_TITLE", "Список обрабатываемых запросов");

        $template->set_filenames(array(
                'body' => 'viewgarant_mine.html')
        );
        break;
}
switch ($type){
    case 'create_invoice':
        $template->assign_var("L_TITLE", "Создание счета для оплаты");

        $template->set_filenames(array(
                'body' => 'viewgarant_invoice.html')
        );
        break;
}

make_jumpbox(append_sid("{$phpbb_root_path}viewforum.$phpEx"));

page_footer();

