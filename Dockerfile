FROM php:5.6-apache

MAINTAINER s3rius_san <win10@list.ru>

RUN apt-get update && \
    apt-get install -y zip \
                       unzip \
                       git \
                       libffi-dev \
                       unixodbc-dev \
                       wget



##RUN apt-get remove --purge apache2 apache2-utils

##RUN apt-get update && apt-get install -y build-essential



RUN echo "DocumentRoot \"/var/www/html\"" >> /etc/apache2/apache2.conf
##RUN apt-get install -y php5.6-gd


WORKDIR /var/www

RUN php -r "readfile('https://getcomposer.org/installer');" | php

RUN echo "HttpProtocolOptions unsafe" >> /etc/apache2/apache2.conf
RUN echo "ServerName forum.gaming-warehouse.com:80" >> /etc/apache2/apache2.conf

ADD forum/composer.json composer.json

ADD Request.php Request.php
#ADD forum/composer.lock composer.lock
#ADD forum/includes includes


RUN php composer.phar install



RUN cp Request.php vendor/symfony/http-foundation/Symfony/Component/HttpFoundation/Request.php
RUN docker-php-ext-install mysqli && \
    docker-php-ext-enable mysqli

##RUN chown -R www-data:www-data /var/www/html

RUN a2enmod rewrite
##RUN service apache2 restart

WORKDIR /var/www/html
##CMD ["/usr/sbin/apache2ctl", "-D","FOREGROUND"]
